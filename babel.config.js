const GoogleFontsPlugin = require("google-fonts-webpack-plugin")

module.exports = {
  "entry": "main.js",

  plugins: [
    new GoogleFontsPlugin({
      fonts: [
        { family: "Satisfy" },
        { family: "Dancing Script" },
        { family: "Roboto", variants: [ "400", "700italic" ] }
      ]

    })
  ],

  presets: [
    '@vue/app'
  ]
}
