import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import store from "./store"
import axios from "axios"
import ArgonDashboard from './plugins/argon-dashboard'
import vuetify from './plugins/vuetify';
import Vuelidate from 'vuelidate'
import VueGoodTablePlugin from 'vue-good-table';
import Notifications from "vue-notification";
import VueRouterUserRoles from "vue-router-user-roles";
import VueSession from 'vue-session';
import VueSweetalert2 from 'vue-sweetalert2';

// import the styles
import 'vue-good-table/dist/vue-good-table.css'
import 'sweetalert2/dist/sweetalert2.min.css'

Vue.config.productionTip = false;

Vue.use(Vuelidate);
Vue.use(ArgonDashboard);
Vue.use(VueGoodTablePlugin);
Vue.use(Notifications);
Vue.use(VueRouterUserRoles, {router});
Vue.use(VueSession, {persist: true});
Vue.use(VueSweetalert2);

//mixins
import LoaderMixin from "./mixins/loader"
import MemberMixin from "./mixins/memberVuex"
import LoanMixin from "./mixins/loanVuex"
import UserTypeMixin from "./mixins/userType"

Vue.mixin(LoaderMixin);
Vue.mixin(MemberMixin);
Vue.mixin(LoanMixin);
Vue.mixin(UserTypeMixin);

//axios
import {authentication_url} from './vuex/urls'
axios.interceptors.request.use(function (config){
  store.dispatch('loader_true')

  if(config.url!== authentication_url){
    config.headers = {'Authorization': 'Bearer ' + localStorage.getItem('access_token')}
  }
  return config
}, function (error) {

    }
)

axios.interceptors.response.use(function (response) {
  store.dispatch('loader_false')
  if (response.status < 400){
    if (response.data){
      Vue.notify({title: 'Success', text: response.data.message, type:'info'})
    }
  }
  return response
}, function (error) {
  store.dispatch('loader_false')
  let errors = error.response.data
  if(errors.hasOwnProperty('message')){
    Vue.notify({title: 'Error message', text: errors.message, type:'warning'})
  }
  return Promise.reject(error)
    }
)


let authenticate = Promise.resolve({ role: "guest" });
authenticate.then(user => {
  Vue.prototype.$user.set(user);
  new Vue({
    render: h => h(App),
    router,
    vuetify,
    store,
    axios,
  }).$mount("#app");
});

