
export default {
    computed: {
        specificLoan() {
            return this.$store.state.LoanStore.specificloanDetails;
        },
        allLoans() {
            return this.$store.state.LoanStore.allloansDetails;
        },
        otherLoan() {
            return this.$store.state.LoanStore.loanDetails;
        },
        pendingLoans() {
            return this.$store.state.LoanStore.pendingLoans;
        },
        acceptedLoans(){
            return this.$store.state.LoanStore.allAcceptedLoans;
        },
        statements(){
            return this.$store.state.LoanStore.statements;
        },
        loanStatement(){
            return this.$store.state.LoanStore.loanStatement;
        },
        userLoan(){
            return this.$store.state.LoanStore.userLoan;
        },
        userRejected(){
            return this.$store.state.LoanStore.userRejected;
        },
        allRejected(){
            return this.$store.state.LoanStore.allRejected;
        },
        guarantorLoans(){
            return this.$store.state.LoanStore.guarantorLoans;
        },

    },
};
