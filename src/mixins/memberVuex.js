
export default {
    computed: {
        specificMember() {
            return this.$store.state.MemberStore.memberDetails;
        },
        allMembers() {
            return this.$store.state.MemberStore.allmemberDetails;
        },
        notifications() {
            return this.$store.state.AuthenticationStore.notifications;
        },
        summary() {
            return this.$store.state.AuthenticationStore.summary;
        },
        userSummary() {
            return this.$store.state.AuthenticationStore.userSummary;
        },

    },
};
