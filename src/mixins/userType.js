export default {
    data(){
      return{
        //the user types constants
        Member : '2',
        Admin : '1'
      }
    },
  
    computed: {
      /*
      * decode the different user types
      */
      decodeUserType: function () {
        let encodedUser = localStorage.getItem('mbnvscz');
        if(encodedUser === 'ldMJ0SQaC8D12Uzn'){
          return this.Admin
        }else if (encodedUser === 'ZwDMWejweQ3F3xut'){
          return this.Member
        }
      },
  
      //return authorized user as admin
      authorizeAdminOnly: function(){
        return this.Admin
      },
  
      //return authorized user as member
      authorizeMemberOnly: function(){
        return this.Member
      },
    },
    methods: {
      /**
       * if user type is not authorized redirect
       * NB: You can't pass parameters to computed parameters hence a method
       */
      authorizedUser: function (authorizedUserType) {
        if(this.decodeUserType !== authorizedUserType){
          this.$router.push({ name: 'login'})
        }
      }
    }
  }
  