import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
Vue.use(Router)

export default new Router({
  mode: 'history',
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: () => import('./views/Login.vue')
        },
        {
          path: '/register',
          name: 'register',
          component: () => import('./views/Register.vue')
        },
        {
          path: '/forgotPassword',
          name: 'forgotPassword',
          component: () => import('./views/ForgotPassword.vue')
        },
        {
          path: '/resetPassword',
          name: 'resetPassword',
          component: () => import('./views/ResetPassword.vue')
        }
      ]
    },
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import('./views/Officials/Dashboard.vue')
        },
        {
          path: '/loanApplication',
          name: 'Loan Application Form ',
          meta: { requiresAuth: true },
          component: () => import('./views/Applicant/LoanApplicationForm.vue')
        },
        {
          path: '/loanStatus',
          name: 'My Loan Applications ',
          meta: { requiresAuth: true },
          component: () => import('./views/Applicant/LoanStatus.vue')
        },
        {
          path: '/loanRepayment',
          name: 'Loan Repayment Statements ',
          component: () => import('./views/Applicant/LoanRepayment.vue')
        },
        {
          path: '/loanApplications',
          name: 'New Loan Applications ',
          meta: { requiresAuth: true },
          component: () => import('./views/Officials/LoanApplications.vue')
        },
        {
          path: '/loanPayments',
          name: 'Loan Payments ',
          component: () => import('./views/Officials/LoanPayments.vue')
        },
        {
          path: '/loanSummary',
          name: 'Loan Summary ',
          component: () => import('./views/Applicant/loanSummary.vue')
        },
        {
          path: '/userDashboard',
          name: 'Dashboard ',
          component: () => import('./views/Applicant/Dashboard.vue')
        },
        {
          path: '/approvedLoans',
          name: 'Approved Loans ',
          meta: { requiresAuth: true },
          component: () => import('./views/Applicant/ApprovedLoans.vue')
        },
        {
          path: '/rejectedLoans',
          name: 'Rejected Loans ',
          meta: { requiresAuth: true },
          component: () => import('./views/Applicant/RejectedLoans.vue')
        },
        {
          path: '/guarantorLoans',
          name: 'Guarantor Pending Loans ',
          meta: { requiresAuth: true },
          component: () => import('./views/Applicant/GuarantorLoans.vue')
        },
        {
          path: '/allRejectedLoans',
          name: 'All Rejected Loans ',
          meta: { requiresAuth: true },
          component: () => import('./views/Officials/RejectedLoans.vue')
        },
        {
          path: '/allLoans',
          name: 'All Loans ',
          meta: { requiresAuth: true },
          component: () => import('./views/Officials/addPayments.vue')
        },
        {
          path: '/registerMember',
          name: 'Register Members ',
          meta: { requiresAuth: true },
          component: () => import('./views/Officials/registerMember.vue')
        },
        {
          path: '/allApplications',
          name: 'All Loan Applications ',
          meta: { requiresAuth: true },
          component: () => import('./views/Officials/allApplications.vue')
        },
        {
          path: '/saccoStatement',
          name: 'Saccos Statement ',
          meta: { requiresAuth: true },
          component: () => import('./views/Officials/saccoStatement.vue')
        },
        {
          path: '/singleLoanSummary',
          name: 'Single Loan Summary ',
          component: () => import('./views/Officials/singleLoanSummary.vue')
        },
        {
          path: '/profile',
          name: 'profile',
          meta: { requiresAuth: true },
          component: () => import('./views/Applicant/UserProfile.vue')
        },
      ]
    },

  ]
})
