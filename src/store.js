import Vue from "vue";
import Vuex from "vuex";
import AuthenticationStore from "./vuex/authentication/store"
import LoaderStore from "./vuex/loader/store"
import MemberStore from "./vuex/members/store"
import LoanStore from "./vuex/loans/store"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        AuthenticationStore,
        LoaderStore,
        MemberStore,
        LoanStore
    }
});
