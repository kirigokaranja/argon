import axios from 'axios';
import {authentication_url} from '../urls'
import router from "../../router"

export default {
    login_function(context,data){

        context.dispatch('loader_true')
        axios.post(authentication_url.login, data)
            .then(response=>{
                context.dispatch('loader_false')
                context.commit('GET_STATE',response.data)
                localStorage.setItem('access_token', response.data.access_token)
                localStorage.setItem('email', response.data.user.user_email)
                localStorage.setItem('name', response.data.user.user_names)
                localStorage.setItem('pdis', response.data.user.user_public_id)
                localStorage.setItem('type', response.data.user.user_category)

                var str = response.data.user.user_names;
                var matches = str.match(/\b(\w)/g); 
                var acronym = matches.join(''); 
                localStorage.setItem('acronym', acronym)

                context.dispatch('find_view')
            }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    find_view(context){
        const userType = context.state.session.user.user_category
        const userPublicId = context.state.session.user.user_public_id
        let userTypeIdentifier;
        if(userType == "1"){
            userTypeIdentifier = 'ldMJ0SQaC8D12Uzn'
            router.push('dashboard')
            context.dispatch('statements_function')
            context.dispatch('viewpendingLoans_function')
            context.dispatch('viewallLoans_function')
            context.dispatch('viewallRejectedLoans_function')
            context.dispatch('viewallAcceptedLoans_function')
            context.dispatch('notifications_function', userPublicId)
        }else{
            userTypeIdentifier = 'ZwDMWejweQ3F3xut'
            router.push('userDashboard')
            context.dispatch('viewspecificMember_function', userPublicId)
            context.dispatch('viewspecificLoan_function', userPublicId)
            context.dispatch('viewUserLoan_function', userPublicId)
            context.dispatch('viewUserRejectedLoan_function', userPublicId)
            context.dispatch('viewGuarantorLoan_function', userPublicId)
            
        }
        localStorage.setItem('mbnvscz',userTypeIdentifier)
    },
    forgotpassword_function(context,data){
        context.dispatch('loader_true');
        axios.post(authentication_url.forgotPassword, data).then(response=>{
            context.dispatch('loader_false')
            router.push('login')
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    resetPassword_function(context, data){
        context.dispatch('loader_true');
        axios.post(authentication_url.resetPassword + data.resetId, data).then(response=>{
            context.dispatch('loader_false')
            router.push('login')
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    notifications_function(context, publicId){
        axios.get(`${authentication_url.viewNotifications + publicId}`)
            .then((response) => {
                context.commit('GET_NOTIFICATIONS_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    updateNotification_function(context, data){
        const userPublicId = localStorage.getItem('pdis');
        axios.post(authentication_url.updateNotification + data).then(response=>{
            context.dispatch('loader_false')
            context.dispatch('notifications_function', userPublicId)
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    summary_function(context){
        axios.get(authentication_url.summary)
            .then((response) => {
                context.commit('GET_SUMMARY_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    userSummary_function(context){
        const userPublicId = localStorage.getItem('pdis');
        axios.get(`${authentication_url.userSummary + userPublicId}`)
            .then((response) => {
                context.commit('GET_USER_SUMMARY_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },

}
