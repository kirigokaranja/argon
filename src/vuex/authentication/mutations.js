export default {
    GET_STATE(state, details){
      state.session = details;
    },
    GET_NOTIFICATIONS_DETAILS(state, details){
      state.notifications = details;
    },
    GET_SUMMARY_DETAILS(state, details){
      state.summary = details;
    },
    GET_USER_SUMMARY_DETAILS(state, details){
      state.userSummary = details;
    },
  }
  