import mutations from './mutations'
import actions from './actions'

const state = {
    session: {},
    notifications: {},
    summary: {},
    userSummary: {},
}

export default {
    state, mutations, actions
}
