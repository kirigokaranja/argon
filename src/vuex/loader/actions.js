export default {
    loader_true(context){
        context.commit('CHANGE_LOADER', true);
    },
    loader_false(context){
        context.commit('CHANGE_LOADER', false);
    }
}
