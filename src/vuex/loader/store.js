import mutations from './mutations'
import actions from './actions'

const state = {
loader: false
}

export default {
    state, mutations, actions
}
