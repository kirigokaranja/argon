import axios from 'axios';
import {loans_url, members_url} from '../urls'
import router from "../../router"

export default {
    loanapplication_function(context, data){
        context.dispatch('loader_true');
        const userPublicId = localStorage.getItem('pdis');
        axios.post(loans_url.postLoan + userPublicId, data).then(response=>{
            context.dispatch('loader_false')
            router.push('loanStatus')
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    viewspecificLoan_function(context, userPublicId) {
        axios.get(`${loans_url.viewLoan + userPublicId}`)
            .then((response) => {
                context.commit('GET_SPECIFIC_LOAN_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewallLoans_function(context) {
        axios.get(loans_url.viewLoans)
            .then((response) => {
                context.commit('GET_ALL_LOANS_DETAILS', response.data.data);
            })
            .catch((error) => {

            });
    },
    viewLoan_function(context, loanPublicId) {
        axios.get(`${loans_url.viewLoanwithId + loanPublicId}`)
            .then((response) => {
                context.commit('GET_LOAN_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewpendingLoans_function(context) {
        axios.get(loans_url.pendingLoans)
            .then((response) => {
                context.commit('GET_PENDING_LOANS_DETAILS', response.data.data);
            })
            .catch((error) => {

            });
    },
    makeLoanDecision_function(context, data) {
        context.dispatch('loader_true');
        const loanPublicId = data.public_id;
        axios.post(loans_url.loanDecision + loanPublicId, data).then(response=>{
            router.push('allLoans')
            context.dispatch('loader_false')
            context.dispatch('viewpendingLoans_function')
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    guarantorDecision_function(context, data) {
        context.dispatch('loader_true');
        const guarantorPublicId = data.public_id;
        const userPublicId = localStorage.getItem('pdis');
        axios.post(loans_url.guarantorDecision + guarantorPublicId, data).then(response=>{
            router.push('guarantorLoans')
            context.dispatch('loader_false')
            context.dispatch('viewGuarantorLoan_function', userPublicId)
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    viewallAcceptedLoans_function(context) {
        axios.get(loans_url.viewAcceptedLoans)
            .then((response) => {
                context.commit('GET_ALL_ACCEPTED_LOANS_DETAILS', response.data.data);
            })
            .catch((error) => {

            });
    },
    statements_function(context) {
        axios.get(loans_url.loanStatements)
            .then((response) => {
                context.commit('GET_STATEMENTS_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewLoanDetails_function(context, loanPublicId) {
        axios.get(`${loans_url.viewLoanDetails + loanPublicId}`)
            .then((response) => {
                context.commit('GET_LOAN_MEMBER_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    addPayment_function(context, data){
        const loanPublicId = data.public_id;
        axios.post(loans_url.postPayment + loanPublicId, data).then(response=>{
            context.dispatch('loader_false')
            context.dispatch('viewallAcceptedLoans_function')
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    viewUserLoan_function(context, userPublicId) {
        axios.get(`${loans_url.userLoan + userPublicId}`)
            .then((response) => {
                context.commit('GET_USER_LOAN_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewUserRejectedLoan_function(context, userPublicId) {
        axios.get(`${loans_url.userRejected + userPublicId}`)
            .then((response) => {
                context.commit('GET_USER__REJECTED_LOAN_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewGuarantorLoan_function(context, userPublicId) {
        axios.get(`${loans_url.guarantorLoans + userPublicId}`)
            .then((response) => {
                context.commit('GET_GUARANTOR_LOAN_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewallRejectedLoans_function(context) {
        axios.get(loans_url.allRejected)
            .then((response) => {
                context.commit('GET_REJECTED_LOAN_DETAILS', response.data.data);
            })
            .catch((error) => {

            });
    },
}
