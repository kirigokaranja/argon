export default{
    GET_SPECIFIC_LOAN_DETAILS(state, details) {
        state.specificloanDetails = details;
    },
    GET_ALL_LOANS_DETAILS(state, details) {
        state.allloansDetails = details;
    },
    GET_LOAN_DETAILS(state, details) {
        state.loanDetails = details;
    },
    GET_PENDING_LOANS_DETAILS(state, details) {
        state.pendingLoans = details;
    },
    GET_ALL_ACCEPTED_LOANS_DETAILS(state, details) {
        state.allAcceptedLoans = details;
    },
    GET_STATEMENTS_DETAILS(state, details) {
        state.statements = details;
    },
    GET_LOAN_MEMBER_DETAILS(state, details) {
        state.loanStatement = details;
    },
    GET_USER_LOAN_DETAILS(state, details) {
        state.userLoan = details;
    },
    GET_USER__REJECTED_LOAN_DETAILS(state, details) {
        state.userRejected = details;
    },
    GET_REJECTED_LOAN_DETAILS(state, details) {
        state.allRejected = details;
    },
    GET_GUARANTOR_LOAN_DETAILS(state, details) {
        state.guarantorLoans = details;
    },
};
