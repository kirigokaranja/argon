import mutations from './mutations'
import actions from './actions'

const state = {
    specificloanDetails: [],
    allloansDetails: [],
    loanDetails: [],
    pendingLoans: [],
    allAcceptedLoans: [],
    statements: [],
    loanStatement: {},
    userLoan: [],
    userRejected: [],
    allRejected: [],
    guarantorLoans: []
}

export default {
    state, mutations, actions
}
