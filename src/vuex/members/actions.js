import axios from 'axios';
import {members_url} from '../urls'
import router from "../../router"

export default {
    registermember_function(context,data){
        context.dispatch('loader_true');
        axios.post(members_url.registerMember, data).then(response=>{
            context.dispatch('loader_false')
            router.push('registerMember')
        }).catch(error=>{
            context.dispatch('loader_false')
        })
    },
    viewspecificMember_function(context, publicId) {
        axios.get(`${members_url.viewMember + publicId}`)
            .then((response) => {
                context.commit('GET_SPECIFIC_MEMBER_DETAILS', response.data);
            })
            .catch((error) => {

            });
    },
    viewallMembers_function(context) {
        axios.get(members_url.viewMembers)
            .then((response) => {
                context.commit('GET_ALL_MEMBERS_DETAILS', response.data.data);
            })
            .catch((error) => {

            });
    },

}
