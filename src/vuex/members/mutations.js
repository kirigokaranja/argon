export default  {
    GET_SPECIFIC_MEMBER_DETAILS(state, details) {
        state.memberDetails = details;
    },
    GET_ALL_MEMBERS_DETAILS(state, details) {
        let uid = localStorage.getItem('pdis');

        function findMmberById(member){
            return member.user_public_id == uid;
        }
         var found = details.findIndex(findMmberById);
         details.splice(found, 1);

         state.allmemberDetails = details;

    },
};
