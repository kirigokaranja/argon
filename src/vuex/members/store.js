import mutations from './mutations'
import actions from './actions'

const state = {
    memberDetails: [],
    allmemberDetails: [],
}

export default {
    state, mutations, actions
}
