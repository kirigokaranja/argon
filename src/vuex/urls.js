//const mainUrl = 'https://msaccoapi.kirigokaranja.com/'
const mainUrl = 'http://127.0.0.1:9090/'

export const authentication_url = {
    login: mainUrl + 'login',
    forgotPassword: mainUrl + 'password/reset',
    resetPassword: mainUrl + 'resetPassword/',
    viewNotifications: mainUrl + 'view/notification/',
    updateNotification: mainUrl + 'update/notification/',
    summary: mainUrl + 'summaries',
    userSummary: mainUrl + 'summary/',
}

export const members_url = {
    registerMember: mainUrl + 'member/add',
    viewMember: mainUrl + 'view/member/',
    viewMembers: mainUrl + 'view/members/all'
}

export const loans_url = {
    postLoan: mainUrl + 'loanApplication/add/',
    viewLoan: mainUrl + 'view/loanApplication/',
    viewLoans: mainUrl + 'view/loanApplications/all',
    viewLoanwithId: mainUrl + 'view/specificLoanApplication/',
    pendingLoans: mainUrl + 'view/loanApplications/pending',
    loanDecision: mainUrl + 'loanApplication/decision/',
    viewAcceptedLoans: mainUrl + 'view/loans/all',
    loanStatements: mainUrl + 'statements',
    viewLoanDetails: mainUrl + 'view/members/statement/',
    postPayment: mainUrl + 'add/payment/',
    userLoan: mainUrl + 'view/loan/',
    allRejected: mainUrl + 'view/loanApplications/rejected',
    userRejected: mainUrl + 'view/rejectedLoanApplication/',
    guarantorLoans: mainUrl + 'view/loanGuarantor/',
    guarantorDecision: mainUrl + 'guarantor/recommendation/'
}
